#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <vector> 


using namespace std;

//------------
// CONSTANTS
//------------

const int N = 11;
const int M = 5;

// tags
const int DATA = 0;
const int CONTROL = 1;

// control codes
const int END = 0;
const int CONTINUE = 1;
const int EXIST = 2;
const int NOTEXIST = 3;


//------------
// UTILS
//------------

void log(const char* msg, int procId) {
    printf("\nProc %d: %s\n", procId, msg);
}


void initRandMatrix(int arr[N][M], int min, int max) {
    int delta = max - min;
    srand((unsigned)time(0));

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < M; j++) {
            arr[i][j] = rand() % delta + min;
        }
    }
}


void showMatrix(int arr[N][M]) {
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            printf("%d\t", arr[i][j]);
        }
        printf("\n");
    }
}

void showArray(int* arr, int size) {
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void showVector(vector<int> v) {
    for (int i = 0; i < v.size(); i++) {
        printf("%d ", v[i]);
    }
    printf("\n");
}


//------------
// MAIN
//------------

int main(int argc, char** argv) {

    //common vars
    int controlCode;
    int diagonalElement;
    int procId;
    int procsNum;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &procId);
    MPI_Comm_size(MPI_COMM_WORLD, &procsNum);

    // PROCCESS 0
    if (procId == 0) {

        int minRand = 0;
        int maxRand = 100;
        int matrix[N][M];
        vector<int> x;

        // init
        log("Init matrix", procId);
        initRandMatrix(matrix, minRand, maxRand);
        showMatrix(matrix);

        // main loop
        log("Start calculating diagonal", procId);
        for (int i = 0; i < N && controlCode != NOTEXIST; i += procsNum-1) {

            // sending each proc one row
            for (int id = 1; id < procsNum; id++) {

                // break if no rows
                int rowI = i + id-1;
                if (rowI >= N) {
                    break;
                }

                // send control code to continue
                MPI_Send(&CONTINUE, 1, MPI_INT, id, CONTROL, MPI_COMM_WORLD);

                // send row & index of row 
                MPI_Send(matrix[rowI], M, MPI_INT, id, DATA, MPI_COMM_WORLD);
                MPI_Send(&rowI, 1, MPI_INT, id, DATA, MPI_COMM_WORLD);
            }

            // receiving results of all proc
            for (int id = 1; id < procsNum; id++) {

                // break if no procs
                int rowI = i + id-1;
                if (rowI >= N) {
                    break;
                }

                // break if diagonal end
                MPI_Recv(&controlCode, 1, MPI_INT, id, CONTROL, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                if (controlCode == NOTEXIST) {
                    break;
                }

                // recv deagonal item
                MPI_Recv(&diagonalElement, 1, MPI_INT, id, DATA, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                x.push_back(diagonalElement);
            }
        };
        log("Finish calculating diagonal", procId);

        // send control code to end
        for (int id = 1; id < procsNum; id++) {
            MPI_Send(&END, 1, MPI_INT, id, CONTROL, MPI_COMM_WORLD);
        };

        showVector(x);
    }

    // PROCCESS 1..N
    else {

        int controlCode;
        int row[M];
        int rowI;

        while (true) {

            // recv control code
            MPI_Recv(&controlCode, M, MPI_INT, 0, CONTROL, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            if (controlCode == END) {
                break;
            }

            // recv row
            MPI_Recv(row, M, MPI_INT, 0, DATA, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            // recv index of row
            MPI_Recv(&rowI, 1, MPI_INT, 0, DATA, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            log("Get row", procId);
            showArray(row, M);

            // if diagonal item exists
            if (rowI < M) {
                diagonalElement = row[rowI];
                MPI_Send(&EXIST, 1, MPI_INT, 0, CONTROL, MPI_COMM_WORLD);
                MPI_Send(&diagonalElement, 1, MPI_INT, 0, DATA, MPI_COMM_WORLD);

                log("Send diagonal item", procId);
                printf("%d\n", diagonalElement);
            }
            else {
                log("No diagonal item", procId);
                MPI_Send(&NOTEXIST, 1, MPI_INT, 0, CONTROL, MPI_COMM_WORLD);
            }
        };

    }

    MPI_Finalize();
}
