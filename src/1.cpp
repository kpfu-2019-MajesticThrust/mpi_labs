#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {
    int procId;
    int procsNum;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &procsNum);
    MPI_Comm_rank(MPI_COMM_WORLD, &procId);
    printf("I am proccess %d in group where are %d proccesses\n", procId, procsNum);
    MPI_Finalize();
}