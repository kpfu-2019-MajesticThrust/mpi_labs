/*
Пусть векторы х, у заданы на нулевом процессе.
Написать программу, в которой векторы равными блоками с нулевого процесса пересылаются остальным процессам.
Пересылка данных осуществляется функцией MPI_Send. Все процессы по формуле вычисляют свои элементы искомых массивов.
Каждый процесс отправляет на нулевой процесс посчитанные элементы массивов. В программе реализовать следующие операции вычисления векторов:
a. z[i]=x[i]+y[i];
b. z[i]=x[i]*y[i].
*/

#include <cstdio>
#include <cstring>
#include <stdio.h>
#include <vector>
#include "mpi.h"

using namespace std;

int const MIN_SIZE = 10;

void* sum_mass(int a[], int b[], int size) {
    for (int i = 0; i < size; ++i) {
        a[i] = a[i] + b[i];
    }
}

void* multiply_mass(int a[], int b[], int size) {
    for (int i = 0; i < size; ++i) {
        a[i] = a[i] * b[i];
    }
}

void* show_massiv(int a[], int size) {
    int i;
    for (i = 0; i < size; i++)
        cout << a[i] <<" ";
    cout << "\n";
}

int* int_massiv(int a[], int size)
{
    int i;

    srand((unsigned)time(0));

    for (i = 0; i < size; i++)
        a[i] = 1 + rand() % 100;
    show_massiv(a, size);
}

int* int_massiv_predicted(int a[], int size)
{
    int i;

    srand((unsigned)time(0));

    for (i = 0; i < size; i++)
        a[i] = i + 3;
    show_massiv(a, size);
}

int main(int argc, char **argv) {
    int value;
    MPI_Init(&argc, &argv);

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    if (world_size < 2) {
        printf("Not enough processors! Need at least 2");
        MPI_Finalize();
        return 1;
    }


    if (world_rank == 0) {

        int massSize = (world_size - 1) * MIN_SIZE;
        int *a = new int[massSize];
        int_massiv(a, massSize);

        int *b = new int[massSize];
        int_massiv_predicted(b, massSize);

        int *resultArray = new int[massSize];

        // master
        for (int i = 1; i < world_size; i++)
        {
           MPI_Send(&a[(i - 1) * MIN_SIZE], MIN_SIZE, MPI_INT, i, 0, MPI_COMM_WORLD);
           MPI_Send(&b[(i - 1) * MIN_SIZE], MIN_SIZE, MPI_INT, i, 0, MPI_COMM_WORLD);
        }

        for (int j = 1; j < world_size; j++) {
            MPI_Recv(resultArray + ((j - 1) * MIN_SIZE), MIN_SIZE, MPI_INT, j, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        cout << "result array is \n";
        show_massiv(resultArray, massSize);
    } else {
        // slave

        MPI_Status status;
        MPI_Probe(0, 0, MPI_COMM_WORLD, &status);

        int message_length;
        MPI_Get_count(&status, MPI_INT, &message_length);

        int *arrayA = new int[message_length];
        int *arrayB = new int[message_length];

        MPI_Recv(arrayA, message_length, MPI_INT, 0,
                 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(arrayB, message_length, MPI_INT, 0,
                 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        multiply_mass(arrayA, arrayB, message_length);

        MPI_Send(arrayA, message_length, MPI_INT, 0, 1, MPI_COMM_WORLD);

        delete[] arrayA;
    }

    MPI_Finalize();
    return 0;
}
