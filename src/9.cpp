/*
9. В массиве вещественных чисел вычислить минимальное значение и глобальный
индекс минимального элемента. Использовать функции `MPI_Scatter` и `MPI_Reduce`
с операцией `MPI_MINLOC`.
*/

#include <cstdio>
#include <openmpi/mpi.h>
#include <random>

using namespace std;

const int ARRAY_SIZE = 100;

struct array_elem_pair_t {
  int elem;
  int i;
};

void print_array(array_elem_pair_t *a, int size) {
  for (auto i = 0; i < size; i++) {
    printf("%d ", a[i].elem);
  }
  printf("\n");
}

void init_array(array_elem_pair_t *a, int size) {
  for (auto i = 0; i < size; i++) {
    // [-100..100]
    a[i].elem = rand() % 201 - 100;
    a[i].i = i;
  }
}

int main(int argc, char **argv) {
  MPI_Init(&argc, &argv);

  srand(time(0));

  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  if (world_size != ARRAY_SIZE) {
    if (world_rank == 0) {
      printf("Not enough processors! Need exactly %d\n", ARRAY_SIZE);
    }

    MPI_Finalize();
    return 1;
  }

  array_elem_pair_t *array;
  array_elem_pair_t buf;

  if (world_rank == 0) {
    // init array on master
    array = new array_elem_pair_t[ARRAY_SIZE];
    init_array(array, ARRAY_SIZE);
    print_array(array, ARRAY_SIZE);
  }

  MPI_Scatter(array, 1, MPI_2INT, &buf, 1, MPI_2INT, 0, MPI_COMM_WORLD);
  array_elem_pair_t min_result;
  MPI_Reduce(&buf, &min_result, 1, MPI_2INT, MPI_MINLOC, 0, MPI_COMM_WORLD);

  if (world_rank == 0) {
    printf("Min element: %d at index %d\n", min_result.elem, min_result.i);

    delete[] array;
  }

  MPI_Finalize();
  return 0;
}
