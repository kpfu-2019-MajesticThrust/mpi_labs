/*
Используя блокирующую операцию передачи сообщений (`MPI_Send` и `MPI_Recv`),
выполнить пересылку данных одномерного массива, из процесса с номером 1 на
остальные процессы группы. На процессах получателях до выставления функции
приема (`MPI_Recv`) определить количество принятых данных (`MPI_Probe`).
Выделить память под размер приемного буфера, после чего вызвать функцию
`MPI_Recv`. Полученное сообщение выдать на экран.
*/

#include <cstdio>
#include <cstring>
#include <openmpi/mpi.h>

using namespace std;

int main(int argc, char **argv) {
  MPI_Init(&argc, &argv);

  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  if (world_size < 2) {
    printf("Not enough processors! Need at least 2");
    MPI_Finalize();
    return 1;
  }

  if (world_rank == 0) {
    // master

    const char message[] = "message for slaves";
    for (int i = 0; i < world_size; i++) {
      if (i != world_rank)
        MPI_Send(message, strlen(message) + 1, MPI_CHAR, i, 0, MPI_COMM_WORLD);
    }
  } else {
    // slave

    MPI_Status status;
    MPI_Probe(0, 0, MPI_COMM_WORLD, &status);

    int message_length;
    MPI_Get_count(&status, MPI_CHAR, &message_length);

    char *message_buffer = new char[message_length];
    MPI_Recv(message_buffer, message_length, MPI_CHAR, MPI_ANY_SOURCE,
             MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    printf("Process %d received message: \"%s\"\n", world_rank, message_buffer);

    delete[] message_buffer;
  }

  MPI_Finalize();
  return 0;
}
