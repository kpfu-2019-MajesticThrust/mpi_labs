#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void log(const char* msg, int procId) {
    printf("Proc %d: %s\n", procId, msg);
}


void initRandArray(int* arr, int N, int min, int max) {

    int delta = max - min;

    srand((unsigned)time(0));

    for (int i = 0; i < N; i++) {
        arr[i] = rand() % delta + min;
    }
}


void showArray(int* arr, int N) {

    for (int i = 0; i < N; i++) {
        printf("%d ", arr[i]);
    }
}


int main(int argc, char** argv) {

    int N = 10;
    int minRand = 0;
    int maxRand = 100;
    int arr[N];

    int procId;
    int procsNum;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &procId);
    MPI_Comm_size(MPI_COMM_WORLD, &procsNum);

// PROCCESS 0
    if (procId == 0) {

        // init
        log("Init array", procId);
        initRandArray(arr, N, minRand, maxRand);

        // send
        log("Send array", procId);
        MPI_Send(&arr, N, MPI_INT, 1, 0, MPI_COMM_WORLD);

    }

// PROCCESS 1
    else if (porcId == 1) {

        // recieve
        MPI_Recv(arr, N, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        log("Recieve array", procId);

        // show
        log("Show array", procId);
        showArray(arr, N);
        printf("\n");

    }

    MPI_Finalize();
}
