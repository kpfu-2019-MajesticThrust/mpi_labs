// mpi_test.cpp : Defines the entry point for the console application.
//создаем новый тип данных newtype для посылки сообщения из 10 элементов char и
//одного элемента float. Данные пересылаются с нулевого процесса на первый

#include <openmpi/mpi.h>
#include <stddef.h> /* offsetof */
#include <stdio.h>

struct MyType {
  bool p;
  char c[10];
  int d;
  float f;
};

int main(int argc, char *argv[]) {
  struct MyType my;
  int blocklengths[2] = {10, 1};

  int rank;
  int tag = 33;

  MPI_Init(&argc, &argv);

  MPI_Datatype types[2] = {MPI_CHAR, MPI_FLOAT};
  MPI_Aint address[3];
  MPI_Aint disp[2];
  MPI_Datatype newtype;

  MPI_Get_address(&my, &address[0]);
  MPI_Get_address(&(my.c[0]), &address[1]);
  MPI_Get_address(&(my.f), &address[2]);
  disp[0] = address[1] - address[0];
  disp[1] = address[2] - address[0];

  /* либо можно так получить смещение:
  disp[0] = offsetof(struct MyType, c);
  disp[1] = offsetof(struct MyType, f);
  (так даже красивее)
  */

  MPI_Type_create_struct(2, blocklengths, disp, types, &newtype);
  MPI_Type_commit(&newtype);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (rank == 0) {
    my.c[0] = 'c';
    my.f = 2.0f;
    MPI_Send(&my, 1, newtype, 1, tag, MPI_COMM_WORLD);
  } else if (rank == 1) {
    MPI_Status status;
    MPI_Recv(&my, 1, newtype, 0, tag, MPI_COMM_WORLD, &status);
    printf("recv rank=1 c=%c f=%f\n", my.c[0], my.f);
  }
  MPI_Type_free(&newtype);
  MPI_Finalize();
  return 0;
}
